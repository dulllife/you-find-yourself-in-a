module YouFindYourselfInA.Console where

import Prelude

import Data.Either (Either(Right))
import Data.String (take, length)
import Effect.Class (liftEffect)
import Effect.Aff (Aff, nonCanceler, makeAff)
import Effect.Console (log)

import Node.Process (stdin)
import Node.Stream (onDataString)
import Node.Encoding (Encoding(UTF8))

say :: String -> Aff Unit
say = (liftEffect <<< log)

dropLast :: String -> String
dropLast x = take ((length x) - 1) x

ask :: Aff String
ask = makeAff go 
  where
        go handler = onDataString stdin UTF8 (handler <<< Right <<< dropLast) $> nonCanceler
