module YouFindYourselfInA.GameState where

import Prelude
import Data.List (List)
import Data.HashMap (HashMap, empty)

type ActionId
  = Int

type PageId
  = Int

data Action
  = SimpleAction
    { outcomePageId :: PageId -- Must exist in the game
    , id :: ActionId -- Must be unique within a Page
    , description :: String
    , preDescription :: String
    , postDescription :: String
    }

data Page
  = Page
    { id :: PageId -- Must be unique within the game
    , description :: String
    , preDescription :: String
    , postDescription :: String
    , actions :: List Action
    }

data World
  = World
    { pages :: HashMap PageId Page
    , initialPageId :: PageId
    }

data Player
  = Player
    { name :: String
    , currentPage :: PageId
    , attributes :: HashMap String Int
    }

data GameState
  = GameState
    { player :: Player
    , world :: World
    }

instance showAction :: Show Action where
  show (SimpleAction sa) = "<Action " <> (show sa.id) <> ">"

instance showPage :: Show Page where
  show (Page p) = "<Page " <> (show p.id) <> ">"

createPlayerState :: String -> PageId -> Player
createPlayerState name initialPageId = Player { name: name, currentPage: initialPageId, attributes: empty }
