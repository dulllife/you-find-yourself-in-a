module YouFindYourselfInA.ContentHelpers where

import Prelude
import Data.List as L
import Effect.Ref as Ref
import Effect (Effect)
import YouFindYourselfInA.GameState (Page(..), PageId, Action(..))

pageIdCounter :: Effect (Ref.Ref Int)
pageIdCounter = Ref.new 0

actionIdCounter :: Effect (Ref.Ref Int)
actionIdCounter = Ref.new 0

genId :: Effect (Ref.Ref Int) -> Effect Int
genId = (=<<) $ Ref.modify $ add 1

genPageId :: Effect Int
genPageId = genId pageIdCounter

genActionId :: Effect Int
genActionId = genId actionIdCounter

page :: String -> Effect Page
page description = do
  pid <- genPageId
  pure
    $ Page
        { id: pid
        , description: description
        , preDescription: ""
        , postDescription: ""
        , actions: L.fromFoldable []
        }

action :: String -> PageId -> Effect Action
action description outcomePageId = do
  aid <- genActionId
  pure
    $ SimpleAction
        { outcomePageId: outcomePageId
        , id: aid
        , description: description
        , preDescription: ""
        , postDescription: ""
        }

addAction :: Action -> Page -> Page
addAction a (Page p) = Page $ p { actions = (a L.: p.actions) }

addAction' :: String -> PageId -> Page -> Effect Page
addAction' description outcomePageId (Page p) = do
  a <- action description outcomePageId
  pure $ Page $ p { actions = (a L.: p.actions) }
