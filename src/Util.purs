module YouFindYourselfInA.Utils where

import Prelude

import Effect
import Data.Maybe (Maybe)
import Data.List (List, index, length)
import Effect.Random (randomInt)

choice :: forall a. List a -> Effect (Maybe a)
choice items = do
  i <- randomInt 0 $ (length items) - 1
  pure $ index items i

