module Main where

import Prelude

import Data.List (List, fromFoldable)
import Data.Maybe (fromMaybe)
import Data.String (null)
import Effect (Effect)
import Effect.Aff (Aff, launchAff_)
import Effect.Class (liftEffect)
import Node.Process (exit)

import YouFindYourselfInA.Console (ask, say)
import YouFindYourselfInA.Utils (choice)

namesList :: List String
namesList = fromFoldable ["Esphyr", "Soheil", "Azmin", "Ashwini"]

randomName :: Effect String
randomName = do
  chosen <- choice namesList
  pure $ fromMaybe "DEFAULT" chosen

doStuff :: Aff Unit
doStuff = do
  randomName1 <- liftEffect $ randomName
  say $ "What is a name?"
  name <- ask

  say $ "Name: " <> (if (null name)
                       then randomName1
                       else name)

  liftEffect $ exit 0

main :: Effect Unit
main = launchAff_ doStuff
